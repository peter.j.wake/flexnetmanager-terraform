terraform {
  required_version = ">= 0.12"

  required_providers {
    aws = ">= 2.0"
  }

  backend "s3" {
    bucket         = "rs-martian-prod-terraform-state"
    key            = "rs-martian-infra/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "rs-martian-prod-terraform-locks"
  }
}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}
